; hello with name
(define hello
    (lambda (name)
        (string-append "Hello" name "!")))

; sum of three numbers
(define sum3
  (lambda (a b c)
    (+ a b c)))


(define add1
    (lambda (x)
        (+ x 1)))

(define sub1
    (lambda (x)
        (- x 1)))


(define (absval x)
    (if (> x 0)
        x
        (* x -1)))

